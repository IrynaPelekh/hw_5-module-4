use ip_library
go

drop table if exists author_log
go

create table author_log(
operation_id int NOT NULL IDENTITY PRIMARY KEY,
author_id_new int null,
name_new varchar(50) null,
URL_new varchar(50) null,
author_id_old int null,
name_old varchar(50) null,
URL_old varchar(50) null,
operation_type varchar(1) check (operation_type in('i','u','d')) not null,
operation_datetime datetime default (getdate()) not null
)
