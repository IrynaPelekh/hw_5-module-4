use ip_library
go

DROP VIEW IF EXISTS author_book_view
go

CREATE VIEW author_book_view
AS
SELECT * FROM author_book
go

SELECT * FROM author_book_view
go