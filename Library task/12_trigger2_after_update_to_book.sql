use ip_libary
go


drop trigger if exists tr3_book
go

create trigger tr3_book ON book
after update
as
begin
	update book
	set updated = getdate(), updated_by = SYSTEM_USER
	from book
		inner join inserted on book.ISBN = inserted.ISBN
end
go