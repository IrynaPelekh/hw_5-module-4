use ip_library
go

DROP SYNONYM IF EXISTS author_book_synonym
go


CREATE SYNONYM dbo.author_book_synonym FOR ip_library.dbo.author_book
go