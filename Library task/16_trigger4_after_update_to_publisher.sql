use ip_library
go


drop trigger if exists tr5_publisher
go

create trigger tr5_publisher ON publisher
after update
as
begin
	update publisher
	set updated = getdate(), updated_by = SYSTEM_USER
	from publisher
		inner join inserted on publisher.publisher_id = inserted.publisher_id
end
go
