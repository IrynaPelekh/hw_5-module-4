use ip_library;
go


drop trigger if exists tr_author_log 
go

create trigger tr_author_log ON author
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
set @operation = 
case 
	when @ins > 0 and @del > 0 then 'U'  
	when @ins = 0 and @del > 0 then 'D'  
	when @ins > 0 and @del = 0 then 'I'  
end

---- insert
if @operation = 'I'
begin
	insert into author_log
				(author_id_new,name_new,URL_new,operation_type)
	Select 
		inserted.author_id,
		inserted.name,
		inserted.URL,
		@operation
	FROM author 
	inner join inserted on author.author_id = inserted.author_id
end

---- delete
if @operation = 'D'
begin
	insert into author_log
				(author_id_old,name_old,URL_old,operation_type)
	Select 
		deleted.author_id,
		deleted.name,
		deleted.URL,
		@operation
	FROM deleted
end

--- update
if @operation = 'U'
	begin
		update authors 
		set updated = getdate(), 
			updated_by = SYSTEM_USER
		from author 
			inner join inserted on author.author_id = inserted.author_id;

	insert into author_log
				(author_id_new,name_new,URL_new,author_id_old,name_old,URL_old,operation_type)
	Select 
		inserted.author_id,
		inserted.name,
		inserted.URL,
		deleted.author_id,
		deleted.name,
		deleted.URL,
		@operation
	FROM inserted INNER JOIN deleted ON inserted.author_id = deleted.author_id
		
	end
END;
GO
