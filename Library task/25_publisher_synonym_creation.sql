use ip_library
go

DROP SYNONYM IF EXISTS publisher_synonym
go


CREATE SYNONYM dbo.publisher_synonym FOR ip_library.dbo.publisher
go