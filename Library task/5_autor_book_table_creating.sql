use ip_library
go

drop table if exists author_book
go

create table author_book(
author_book_id int NOT NULL default(1) check(author_book_id>=1),
author_id int NOT NULL,
ISBN varchar(17) NOT NULL,
seq_no int  default(1) check (seq_no>=1) not null,
inserted date not null default getdate(),
inserted_by varchar(50) not null default system_user,
updated	date NULL,
updated_by varchar(50) NULL,

CONSTRAINT PK_author_book PRIMARY KEY(author_book_id),

CONSTRAINT FK_ISBN FOREIGN KEY(ISBN) 
REFERENCES book(ISBN) ON UPDATE CASCADE ON DELETE NO ACTION,

CONSTRAINT FK_author_id FOREIGN KEY(author_id) 
REFERENCES author(author_id) ON UPDATE CASCADE ON DELETE NO ACTION,

CONSTRAINT UK_ISBN_Authors
  UNIQUE(ISBN,author_id)
)