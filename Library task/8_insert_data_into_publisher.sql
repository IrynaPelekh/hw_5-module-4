use ip_library
go

insert into publisher
(publisher_id, name, URL,  updated, updated_by) values 
(NEXT VALUE FOR seq2_ip_library,'Bukva', 'www.bukva.ua', NULL, NULL),
(NEXT VALUE FOR seq2_ip_library,'Black_and_White_Publishing', 'www.blackandwhitepublishing.com', NULL, NULL),
(NEXT VALUE FOR seq2_ip_library,'Legend_Press', 'www.legendtimesgroup.co.uk', NULL, NULL),
(NEXT VALUE FOR seq2_ip_library,'Macmillan_Publishers', 'www.us.macmillan.com', NULL, NULL),
(NEXT VALUE FOR seq2_ip_library,'Mercury_House', 'www.mercuryhouse.org', NULL, NULL)
go
