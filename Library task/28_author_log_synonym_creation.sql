use ip_library
go

DROP SYNONYM IF EXISTS author_log_synonym
go


CREATE SYNONYM dbo.author_log_synonym FOR ip_library.dbo.author_log
go