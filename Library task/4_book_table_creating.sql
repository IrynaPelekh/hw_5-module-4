use ip_library
go

drop table if exists book
go

create table book(
ISBN varchar(17) NOT NULL PRIMARY KEY,
publisher_id int not null,
URL varchar(50) unique not null,
price numeric(7,2) default(0) check (price>=0) not null,
inserted date not null default getdate(),
inserted_by varchar(50) default system_user,
updated	date NULL,
updated_by varchar(50) NULL

CONSTRAINT [FK_book_publisher] FOREIGN KEY ([publisher_id])
	REFERENCES [publisher] ([publisher_id])
on delete cascade
on update cascade
)