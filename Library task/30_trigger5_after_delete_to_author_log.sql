use ip_library;
go

drop trigger if exists tr_delete_author_log; 
go

create trigger tr_delete_author_log ON author_log
after delete
as
begin
rollback tran;
end;