use ip_library
go


drop trigger if exists tr4_author_book
go

create trigger tr4_author_book ON author_book
after update
as
begin
	update author_book
	set updated = getdate(), updated_by = SYSTEM_USER
	from author_book
		inner join inserted on author_book.author_book_id = inserted.author_book_id 
end
go