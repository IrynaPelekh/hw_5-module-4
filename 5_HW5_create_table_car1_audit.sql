USE auto_show1
go

drop table if exists car1_audit
go

CREATE TABLE car1_audit (
  id int not null PRIMARY KEY IDENTITY,   
  model_id int,  
  new_model varchar(60),
  old_model varchar(60),
  drive1 varchar(50),
  height1_mm int
    CONSTRAINT ck_height1_mm CHECK(height1_mm > 0),
  width1_mm int
    CONSTRAINT ck_width1_mm CHECK(width1_mm > 0),
  wheelbase1_mm int
    CONSTRAINT ck_wheelbase1_mm CHECK(wheelbase1_mm > 0),
  length1_mm int
    CONSTRAINT ck_length1_mm CHECK(length1_mm > 0),
  engine_layout1 varchar(30),
  cylinders_number1 int,
  engine_displacement1 int,
  power1_kW int,
  top_speed1_kmh int,
  trunk_capacity1 int,
  fuel_tank1 int,
  color1 varchar(15),
  doors_number1 int,
  price1_usd decimal(15,2),
  special_model1 bit,
  operation_type char(1),
  operation_date datetime not null default getdate()
)
