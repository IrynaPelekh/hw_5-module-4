use auto_show1
go

drop trigger if exists tr_car1 
go

---- trigger creation
create trigger tr_car1 ON car1
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
set @operation = 
case 
	when @ins > 0 and @del > 0 then 'U'  
	when @ins = 0 and @del > 0 then 'D'  
	when @ins > 0 and @del = 0 then 'I'  
end 
---- insert
if @operation = 'I'
begin
	insert into car1_audit 
	    (model_id, operation_type, new_model, drive1,
             height1_mm, width1_mm, wheelbase1_mm, length1_mm,
             engine_layout1, cylinders_number1, engine_displacement1,
             power1_kW, top_speed1_kmh, trunk_capacity1, fuel_tank1,
             color1, doors_number1, price1_usd, special_model1)
	Select 
	     inserted.id, @operation, inserted.model,
             inserted.drive, inserted.height_mm, inserted.width_mm,
             inserted.wheelbase_mm, inserted.length_mm,
             inserted.engine_layout, inserted.cylinders_number,
             inserted.engine_displacement, inserted.power_kW,
             inserted.top_speed_kmh, inserted.trunk_capacity, 
             inserted.fuel_tank, inserted.color, inserted.doors_number,
             inserted.price_usd, inserted.special_model

	FROM car1 
	inner join inserted on car1.id = inserted.id
end
---- delete
if @operation = 'D'
begin
	insert into car1_audit (model_id, operation_type, 
             old_model, drive1, height1_mm, width1_mm, wheelbase1_mm, length1_mm,
             engine_layout1, cylinders_number1, engine_displacement1,
             power1_kW, top_speed1_kmh, trunk_capacity1, fuel_tank1,
             color1, doors_number1, price1_usd, special_model1)
	Select 
	     deleted.id, @operation, deleted.model,
             deleted.drive, deleted.height_mm, deleted.width_mm,
             deleted.wheelbase_mm, deleted.length_mm,
             deleted.engine_layout, deleted.cylinders_number,
             deleted.engine_displacement, deleted.power_kW,
             deleted.top_speed_kmh, deleted.trunk_capacity, 
             deleted.fuel_tank, deleted.color, deleted.doors_number,
             deleted.price_usd, deleted.special_model
	FROM deleted 
end
--- update
if @operation = 'U'
	begin
		update car1 
		set updated = getdate(), 
			updatecnt += 1
		from car1 
			inner join inserted on car1.id = inserted.id

		if update(drive)
		    insert into car1_audit 
	            (model_id, operation_type, old_model,new_model,
                    drive1, height1_mm, width1_mm, wheelbase1_mm, length1_mm,
                    engine_layout1, cylinders_number1, engine_displacement1,
                    power1_kW, top_speed1_kmh, trunk_capacity1, fuel_tank1,
                    color1, doors_number1, price1_usd, special_model1)
			select
			   inserted.id, @operation, deleted.model, inserted.model,
       			'old drive value: ' + isnull(deleted.drive,''),
                        inserted.height_mm, inserted.width_mm, inserted.wheelbase_mm, 
                        inserted.length_mm, inserted.engine_layout, 
                        inserted.cylinders_number, inserted.engine_displacement, 
                        inserted.power_kW, inserted.top_speed_kmh, 
                        inserted.trunk_capacity, inserted.fuel_tank, inserted.color, 
                        inserted.doors_number, inserted.price_usd, inserted.special_model
                        
			FROM car1 
				inner join  inserted on car1.id = inserted.id
				inner join deleted on car1.id = deleted.id
		else
			insert into car1_audit 
				(model_id, operation_type, old_model,new_model)
			select
				inserted.id,
				@operation,
				deleted.model,
				inserted.model
			FROM trg.car1
				inner join  inserted on car1.id = inserted.id
				inner join deleted on car1.id = deleted.id
	end

END
GO