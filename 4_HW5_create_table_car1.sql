USE auto_show1
go

drop table if exists car1
go

CREATE TABLE car1 (
  id int PRIMARY KEY IDENTITY,     
  model varchar(60),
  drive varchar(50),
  height_mm int
    CONSTRAINT ck_height_mm CHECK(height_mm > 0),
  width_mm int
    CONSTRAINT ck_width_mm CHECK(width_mm > 0),
  wheelbase_mm int
    CONSTRAINT ck_wheelbase_mm CHECK(wheelbase_mm > 0),
  length_mm int
    CONSTRAINT ck_length_mm CHECK(length_mm > 0),
  engine_layout varchar(30),
  cylinders_number int,
  engine_displacement int,
  power_kW int,
  top_speed_kmh int,
  trunk_capacity int,
  fuel_tank int,
  color varchar(15),
  doors_number int,
  price_usd decimal(15,2),
  special_model bit,
  created datetime not null default getdate(),
  updated datetime,
  updatecnt int not null default 0
)
