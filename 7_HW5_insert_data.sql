-- test  triggers
use auto_show1
go

INSERT INTO car1 
(id,model,height_mm,width_mm,wheelbase_mm,length_mm,
engine_layout, cylinders_number,engine_displacement,
power_kW,top_speed_kmh,trunk_capacity,fuel_tank,color,
doors_number,price_usd,special_model,updated)

VALUES  (1,'Porsche Panamera',
1423,1937,2950,5049,'Front Engine',6,2995,243,264,
500,75,'black',5,115000,0, null),
(2,'Porsche Panamera 4',1428,1937,
3100,5199,'Front Engine',6,2995,243,278,500,75,'white',
5,117748,1, null)

INSERT INTO car1 
(id,model,drive,height_mm,width_mm,wheelbase_mm,length_mm,
engine_layout, cylinders_number,engine_displacement,
power_kW,top_speed_kmh,trunk_capacity,fuel_tank,color,
doors_number,price_usd,special_model,updated)

VALUES (3,'Porsche Panamera 4 Sport Turismo','All while drive',
1428,1937,2950,5049,'Front Engine',6,2995,243,264,520,75,
'black',5,121720,1,null),

go

select * from car1
go
select * from car1_audit
go
