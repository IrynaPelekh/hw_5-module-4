use auto_show
go

CREATE OR ALTER VIEW customer_view
      (car_id, quantity, customer_price_usd, 
       client_id, order_price)
AS
SELECT car_id, quantity, customer_price_usd, 
       client_id, order_price
FROM customer